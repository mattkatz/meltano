# Careers

Want to be help create a new product and make an impact in the world of data? You're in luck because we're hiring!

## Open Positions

Here are the open positions that we currently have:

- [General Manager](https://boards.greenhouse.io/gitlab/jobs/4059274002)
- [Senior Product Manager](https://boards.greenhouse.io/gitlab/jobs/4059265002)

## Benefits

For our benefits, please check out the [GitLab Handbook](https://about.gitlab.com/handbook/benefits/) for more information.
